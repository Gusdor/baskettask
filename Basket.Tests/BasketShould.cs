using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace Basket.Tests
{
    [TestClass]
    public class BasketShould
    {
        /// <summary>
        /// Basket 1:
        /// 1 Hat @ �10.50
        /// 1 Jumper @ �54.65
        /// ------------
        /// 1 x �5.00 Gift Voucher XXX-XXX applied
        /// ------------
        /// Total: �60.15
        /// </summary>
        [TestMethod]
        public void DiscountTotalWhenGiftVoucherIsApplied()
        {
            var basket = new ShoppingBasket();

            var hat = new Item("Hat", 10.5);
            var jumper = new Item("Jumper", 54.65);

            basket.Products.Add(hat);
            basket.Products.Add(jumper);

            var giftVoucher = new GiftVoucher("XXX-XXX", 5.0);
            basket.Vouchers.Add(giftVoucher);

            var result = basket.Totalise();
            result.IsValid.Should().BeTrue();
            result.Total.Should().Be(60.15);
        }

        /// <summary>
        /// Basket 2:
        /// 1 Hat @ �25.00
        /// 1 Jumper @ �26.00
        /// ------------
        /// 1 x �5.00 off Head Gear in baskets over �50.00 Offer Voucher YYY-YYY applied
        /// ------------
        /// Total: �51.00
        /// Message: �There are no products in your basket applicable to voucher Voucher YYY-YYY.�
        /// </summary>
        [TestMethod]
        public void BeInvalidIfNoItemsCanBeDiscountedByAnOfferVoucher()
        {
            var basket = new ShoppingBasket();

            var hat = new Item("Hat", 25.0);
            var jumper = new Item("Jumper", 26.0);

            basket.Products.Add(hat);
            basket.Products.Add(jumper);

            var offerVoucher = 
                new OfferVoucher("YYY-YYY", 5.0, category: "Head Gear", priceThreshold: 50.0);

            basket.Vouchers.Add(offerVoucher);

            var result = basket.Totalise();
            result.IsValid.Should().BeFalse();
            result.Total.Should().Be(51.0);
            result.ValidationError.Should()
                .Be("There are no products in your basket applicable to voucher Voucher YYY-YYY.");
        }

        /// <summary>
        /// Basket 3:
        /// 1 Hat @ �25.00
        /// 1 Jumper @ �26.00
        /// 1 Head Light(Head Gear Category of Product) @ �3.50
        /// ------------
        /// 1 x �5.00 off Head Gear in baskets over �50.00 Offer Voucher YYY-YYY applied
        /// ------------
        /// Total: �51.00
        /// </summary>
        [TestMethod]
        public void DiscountItemsOfTheCorrectCategoryWithAnOfferVoucher()
        {
            var basket = new ShoppingBasket();

            var hat = new Item("Hat", 25.0);
            var jumper = new Item("Jumper", 26.0);
            var headLight = new Item("Head Light", 3.5, category: "Head Gear");

            basket.Products.Add(hat);
            basket.Products.Add(jumper);
            basket.Products.Add(headLight);

            var offerVoucher =
                new OfferVoucher("YYY-YYY", 5.0, category: "Head Gear", priceThreshold: 50.0);

            basket.Vouchers.Add(offerVoucher);

            var result = basket.Totalise();
            result.IsValid.Should().BeTrue();
            result.Total.Should().Be(51.0);
        }

        /// <summary>
        /// Basket 4:
        /// 1 Hat @ �25.00
        /// 1 Jumper @ �26.00
        /// ------------
        /// 1 x �5.00 Gift Voucher XXX-XXX applied
        /// 1 x �5.00 off baskets over �50.00 Offer Voucher YYY-YYY applied
        /// ------------
        /// Total: �41.00
        /// </summary>
        [TestMethod]
        public void ApplyDiscountFromGiftAndOfferVouchers()
        {
            var basket = new ShoppingBasket();

            var hat = new Item("Hat", 25.0);
            var jumper = new Item("Jumper", 26.0);

            basket.Products.Add(hat);
            basket.Products.Add(jumper);

            var giftVoucher = new GiftVoucher("XXX-XXX", 5.0);
            var offerVoucher =
                new OfferVoucher("YYY-YYY", 5.0, priceThreshold: 50.0);

            basket.Vouchers.Add(offerVoucher);
            basket.Vouchers.Add(giftVoucher);

            var result = basket.Totalise();
            result.IsValid.Should().BeTrue();
            result.Total.Should().Be(41.0);
        }

        /// <summary>
        /// Basket 5:
        /// 1 Hat @ �25.00
        /// 1 �30 Gift Voucher @ �30.00
        /// ------------
        /// 1 x �5.00 off baskets over �50.00 Offer Voucher YYY-YYY applied
        /// ------------
        /// Total: �55.00
        /// ------------
        /// Message: �You have not reached the spend threshold for voucher YYY-YYY.Spend another �25.01 to
        /// receive �5.00 discount from your basket total.�
        /// </summary>
        [TestMethod]
        public void BeInvalidIfAnOfferVouchersPriceThresholdIsNotMet()
        {
            var basket = new ShoppingBasket();

            var hat = new Item("Hat", 25.0);
            var giftVoucher = new GiftVoucherItem(30.0);

            basket.Products.Add(hat);
            basket.Products.Add(giftVoucher);

            var offerVoucher = new OfferVoucher("YYY-YYY", 5.0, priceThreshold: 50.0);

            basket.Vouchers.Add(offerVoucher);

            var result = basket.Totalise();
            result.IsValid.Should().BeFalse();
            result.Total.Should().Be(55.0);
            result.ValidationError.Should()
                .Be("You have not reached the spend threshold for voucher YYY-YYY. " +
                "Spend another �25.01 to receive �5.00 discount from your basket total.");
        }
    }
}
