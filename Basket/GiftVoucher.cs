﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Basket
{
    /// <summary>
    /// Can be applied to a basket to decreate the total price. 
    /// </summary>
    public class GiftVoucher: IVoucher
    {
        /// <summary>
        /// Initialises a new instance of <see cref="GiftVoucher"/>.
        /// </summary>
        /// <param name="voucherCode">The code that identifies this voucher.</param>
        /// <param name="value">The maximum discount applied by this voucher.</param>
        public GiftVoucher(string voucherCode, double value)
        {
            if (string.IsNullOrWhiteSpace(voucherCode))
            {
                throw new System.ArgumentException($"{nameof(voucherCode)} cannot be null or whitespace.", nameof(voucherCode));
            }

            VoucherCode = voucherCode;
            Value = value;
        }

        /// <summary>
        /// Gets the code that identifies this voucher.
        /// </summary>
        public string VoucherCode { get; }

        /// <summary>
        /// Gets the maximum discount applied by this voucher
        /// </summary>
        public double Value { get; }

        public TotalResult CalculateDiscount(IEnumerable<IItem> items)
        {
            var discountable = items.Where(p => p.IsDiscountable).Sum(p => p.Price);
            var total = discountable - Math.Max(0, discountable - Value);

            return new TotalResult(true, total, null);
        }
    }
}