﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Basket
{
    /// <summary>
    /// Can be applied to a basket to decreate the total price. 
    /// </summary>
    public class OfferVoucher : IVoucher
    {
        /// <summary>
        /// Initialises a new instance of <see cref="OfferVoucher"/>.
        /// </summary>
        /// <param name="voucherCode">The code that identifies this voucher.</param>
        /// <param name="value">The maximum discount applied by this voucher.</param>
        /// <param name="category">The category of <see cref="IItem"/> that is discountable by this voucher.</param>
        /// <param name="priceThreshold">The discountable value that must exist in the basket for the voucher to be valid.</param>
        public OfferVoucher(string voucherCode, double value, string category = null, double priceThreshold = 0.0)
        {
            if (string.IsNullOrWhiteSpace(voucherCode))
            {
                throw new ArgumentException($"{nameof(voucherCode)} cannot be null or whitespace.", nameof(voucherCode));
            }

            VoucherCode = voucherCode;
            Value = value;
            Category = category;
            PriceThreshold = priceThreshold;
        }

        /// <summary>
        /// Gets the code that identifies this voucher.
        /// </summary>
        public string VoucherCode { get; }

        /// <summary>
        /// Gets the maximum discount applied by this voucher
        /// </summary>
        public double Value { get; }

        /// <summary>
        /// Gets the category of <see cref="IItem"/> that is discountable by this voucher.
        /// </summary>
        /// <remarks>If the category is null, all items are discountable.</remarks>
        public string Category { get; }

        /// <summary>
        /// Gets the discountable value that must exist in the basket for the voucher to be valid.
        /// </summary>
        public double PriceThreshold { get; }

        /// <summary>
        /// Filters an item by category. If this offer has a null category, all items are valid.
        /// </summary>
        /// <param name="item">THe item to filter.</param>
        /// <returns>
        /// <c>true</c> if a item has a category that matches this offer, or if this offer has no category specified; otherwise <c>false</c>.
        /// </returns>
        private bool categoryFilter(IItem item)
        {
            if (Category == null)
            {
                return true;
            }
            else
            {
                return item.Category == Category;
            }
        }

        public TotalResult CalculateDiscount(IEnumerable<IItem> items)
        {
            var discountable = items.Where(p => p.IsDiscountable).Sum(p => p.Price);

            var discountableItems = items.Where(p => p.IsDiscountable && categoryFilter(p));

            // Items of the correct category must exist in the basket
            if (!discountableItems.Any())
            {
                return new TotalResult(false, discountable, $"There are no products in your basket applicable to voucher Voucher {VoucherCode}.");
            }

            // The discountable value of the basket must exceed the price threshold
            if(discountable <= PriceThreshold)
            {
                var difference = PriceThreshold - discountable;
                return new TotalResult(false, discountable, 
                    $"You have not reached the spend threshold for voucher {VoucherCode}. " +
                    $"Spend another £{(difference + 0.01).ToString("F2")} to receive £{Value.ToString("F2")} discount from your basket total.") ;
            }

            discountable = discountableItems.Sum(p => p.Price);

            var total = discountable - Math.Max(0, discountable - Value);

            return new TotalResult(true, total, null);
        }
    }
}
