﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basket
{
    public interface IVoucher
    {
        /// <summary>
        /// Calculates if the voucher can be applied and if true the, resulting discount.
        /// </summary>
        /// <param name="items">The items contained in the basket that this voucher is applied to.</param>
        /// <returns>A result containing the total discount and any validation error.</returns>
        TotalResult CalculateDiscount(IEnumerable<IItem> items);
    }
}
