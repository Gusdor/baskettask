﻿namespace Basket
{
    /// <summary>
    /// Defines a purchasable shopping basket item.
    /// </summary>
    public interface IItem
    {
        /// <summary>
        /// The product category of the item.
        /// </summary>
        string Category { get; }

        /// <summary>
        /// The display friendly name that identifies the item.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The price of the item.
        /// </summary>
        double Price { get; }

        /// <summary>
        /// Indicates if the price of an item contributes towards the discountable total required by offer vouchers.
        /// Only discountable items can have their price reduced by gift vouchers.
        /// </summary>
        bool IsDiscountable { get; }
    }
}