﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Basket
{
    /// <summary>
    /// A shopping basket that totalises the price of products and validate applied vouchers.
    /// </summary>
    public class ShoppingBasket
    {
        /// <summary>
        // The products that have been added to the basket.
        /// </summary>
        public List<IItem> Products { get; } = new List<IItem>();

        /// <summary>
        /// The vouchers that have been applied to the basket.
        /// </summary>
        public List<IVoucher> Vouchers { get; } = new List<IVoucher>();

        /// <summary>
        /// Calculates the backet total.
        /// </summary>
        /// <exception cref="InvalidOperationException">The basket contents are not valid.</exception>
        /// <returns></returns>
        public TotalResult Totalise()
        {
            var results = this.Vouchers.Select(p => p.CalculateDiscount(this.Products.AsReadOnly()));

            var sumBeforeDiscount = Products.Sum(p => p.Price);

            var firstError = results.FirstOrDefault(p => !p.IsValid)?.ValidationError;
            if (firstError != null)
            {
                return new TotalResult(false, Products.Sum(p => p.Price), firstError);
            }
            else
            {
                return new TotalResult(true, Math.Round(sumBeforeDiscount - results.Sum(p => p.Total), 3), null);
            }
        }
    }
}
