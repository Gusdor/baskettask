﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basket
{
    /// <summary>
    /// The results of a totalisation operation.
    /// </summary>
    public class TotalResult
    {
        public TotalResult(bool isValid, double total, string validationError)
        {
            IsValid = isValid;
            Total = total;
            ValidationError = validationError;
        }

        /// <summary>
        /// Gets a value specifying if a total could be calculated.
        /// </summary>
        public bool IsValid { get; }

        /// <summary>
        /// Gets a value specifying the total calculated value.
        /// </summary>
        public double Total { get; }

        /// <summary>
        /// Gets a message that describes a validation error that may have occurred.
        /// </summary>
        public string ValidationError { get; }
    }
}
