﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basket
{
    /// <summary>
    /// A purchasable gift voucher.
    /// </summary>
    public class GiftVoucherItem : IItem
    {
        public static readonly string GiftVoucherCategory = "Gift Voucher";

        /// <summary>
        /// Initialsies a new instance of <see cref="GiftVoucherItem"/>.
        /// </summary>
        /// <param name="value">The value of the gift voucher.</param>
        public GiftVoucherItem(double value)
        {
            Price = value;
        }

        public string Category => GiftVoucherCategory;

        public string Name => $"£{Price} Gift Voucher";

        public double Price { get; private set; }

        public bool IsDiscountable => false;
    }
}
