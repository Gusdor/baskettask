﻿using System;

namespace Basket
{
    /// <summary>
    /// An item that can be added to a shopping basket.
    /// </summary>
    public class Item : IItem
    {
        /// <summary>
        /// Initialises a new <see cref="Item"/>.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="price">The price of the item.</param>
        /// <param name="category">The category that this item belongs to.</param>
        public Item(string name, double price, string category = null)
        {
            Name = name;
            Price = price;
            Category = category;
        }


        public bool IsDiscountable => true;
        public string Name { get; }
        public double Price { get; }
        public string Category { get; }
    }
}
